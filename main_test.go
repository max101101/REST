package main

import (
	"database/sql/driver"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/gorilla/mux"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

type AnyTime struct{}

func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(int64)
	return ok
}

func init() {
	log.SetOutput(ioutil.Discard)
}

func TestRegister(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	mock.ExpectExec("INSERT INTO events").WithArgs("a", AnyTime{}).WillReturnResult(sqlmock.NewResult(1, 1))

	req1, err := http.NewRequest("POST", "/events/a", nil)
	if err != nil {
		t.Fatal(err)
	}

	req2, err := http.NewRequest("POST", "/events/z", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr1 := httptest.NewRecorder()

	rr2 := httptest.NewRecorder()

	events := map[string]bool{
		"a": true,
	}

	h := NewHandler(db, 10, events)

	r := mux.NewRouter()
	r.HandleFunc("/events/{event}", h.Register)

	r.ServeHTTP(rr1, req1)
	if rr1.Code != http.StatusCreated {
		t.Errorf("Insert failed %v", rr1.Code)
	}

	r.ServeHTTP(rr2, req2)
	if rr2.Code != http.StatusBadRequest {
		t.Errorf("Registered unknown event, status code %v", rr2.Code)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Not all mock expectations met")
	}
}

func TestRegisterLimit(t *testing.T) {
	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	req, err := http.NewRequest("POST", "/events/a", nil)
	if err != nil {
		t.Fatal(err)
	}

	events := map[string]bool{
		"a": true,
	}

	h := NewHandler(db, 1, events)

	r := mux.NewRouter()
	r.HandleFunc("/events/{event}", h.RegisterLimit)

	wg := &sync.WaitGroup{}
	flag := false

	mutex := &sync.Mutex{}

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			rr := httptest.NewRecorder()
			r.ServeHTTP(rr, req)
			tmp := StatusBandwidthLimitExceeded
			if rr.Code == tmp {
				mutex.Lock()
				flag = true
				mutex.Unlock()
			}
		}()
	}
	wg.Wait()
	if !flag {
		t.Errorf("Throttling fail")
	}
}

func TestGet(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	mock.ExpectQuery("SELECT name FROM events WHERE").WithArgs(AnyTime{}, AnyTime{}).WillReturnRows(sqlmock.NewRows([]string{"name"}).AddRow("a"))

	req, err := http.NewRequest("GET", "/events/0/0", nil)
	if err != nil {
		t.Fatal(err)
	}

	events := map[string]bool{
		"a": true,
		"b": true,
		"c": true,
	}

	h := NewHandler(db, 10, events)

	rr := httptest.NewRecorder()

	r := mux.NewRouter()
	r.HandleFunc("/events/{timefrom}/{timeto}", h.Get)

	r.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("Unexpected status code %v", rr.Code)
	}

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Errorf("Response body not readed")
	}

	expected := map[string]int{
		"a": 1,
		"b": 0,
		"c": 0,
	}
	var response map[string]int
	err = json.Unmarshal(body, &response)
	if err != nil {
		t.Errorf("Unmarhsall json")
	}

	for key, val := range response {
		if expected[key] != val {
			t.Errorf("results not match: (%d, %d)", expected[key], val)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Not all mock expectations met")
	}
}

func BenchmarkRegisterLimit(b *testing.B) {
	db, _, err := sqlmock.New()
	if err != nil {
		return
	}
	defer db.Close()

	req, err := http.NewRequest("POST", "/events/a", nil)
	if err != nil {
		return
	}

	events := map[string]bool{
		"a": true,
	}

	h := NewHandler(db, 10, events)

	r := mux.NewRouter()
	r.HandleFunc("/events/{event}", h.RegisterLimit)

	for i := 0; i < b.N; i++ {
		rr := httptest.NewRecorder()
		r.ServeHTTP(rr, req)
	}
}
