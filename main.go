package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/gorilla/mux"

	_ "github.com/go-sql-driver/mysql"
)

//mysql docker start

//docker run -p 3306:3306 -v $(PWD):/docker-entrypoint-initdb.d -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_DATABASE=golang -d mysql

const (
	StatusBandwidthLimitExceeded = 509

	//db data source name
	dsn = "root:1234@tcp(localhost:3306)/golang?charset=utf8"

	//maximum number of simultaneous registration requests
	maxConns = 10
)

//http handling help struct
type Handler struct {
	DB          *sql.DB
	Limit       chan struct{}
	KnownEvents map[string]bool
}

//register event
func (h Handler) Register(w http.ResponseWriter, r *http.Request) {
	//parse mux param
	vars := mux.Vars(r)
	event, ok := vars["event"]
	if !ok {
		log.Println("Mux param {event} not parsed")
		http.Error(w, "Mux param not parsed", http.StatusInternalServerError)
		return
	}

	//check event
	if _, ok := h.KnownEvents[event]; !ok {
		log.Println("Unknown event", event)
		http.Error(w, "Unknown event", http.StatusBadRequest)
		return
	}

	//register new event
	_, err := h.DB.Exec(
		"INSERT INTO events (`name`, `time`) VALUES (?, ?)",
		event,
		time.Now().Unix(),
	)
	if err != nil {
		log.Println("Insert DB Error", err)
		http.Error(w, "db error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

//check limit
func (h Handler) RegisterLimit(w http.ResponseWriter, r *http.Request) {
	select {
	case <-h.Limit:
		h.Register(w, r)
		h.Limit <- struct{}{}
	default:
		http.Error(w, "Limit exceeded", StatusBandwidthLimitExceeded)
	}
}

//get events
func (h Handler) Get(w http.ResponseWriter, r *http.Request) {
	//parse mux params
	vars := mux.Vars(r)
	timeFrom, err := strconv.ParseUint(vars["timefrom"], 10, 64)
	if err != nil {
		log.Println("Mux param {timefrom} not parsed")
		http.Error(w, "Mux param not parsed", http.StatusInternalServerError)
		return
	}

	timeTo, err := strconv.ParseUint(vars["timeto"], 10, 64)
	if err != nil {
		log.Println("Mux param {timeto} not parsed")
		http.Error(w, "Mux param not parsed", http.StatusInternalServerError)
		return
	}

	rows, err := h.DB.Query(
		"SELECT name FROM events WHERE time > ? and time < ?",
		timeFrom,
		timeTo,
	)
	if err != nil {
		log.Println("Select DB Error", err)
		http.Error(w, "db error", http.StatusInternalServerError)
		return
	}

	//init response
	response := map[string]int{}
	for k, _ := range h.KnownEvents {
		response[k] = 0
	}

	//count events
	for rows.Next() {
		var event string
		err := rows.Scan(&event)
		if err != nil {
			log.Println("Scan DB error", err)
			http.Error(w, "db error", http.StatusInternalServerError)
			return
		}
		response[event]++
	}
	rows.Close()

	//response in json
	responseJson, err := json.Marshal(response)
	if err != nil {
		log.Println("Json Marshal error", err)
		http.Error(w, "json marshal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(responseJson)
}

func NewHandler(db *sql.DB, maxConns int, events map[string]bool) *Handler {
	//create new handler
	h := &Handler{
		DB:          db,
		Limit:       make(chan struct{}, maxConns),
		KnownEvents: events,
	}

	//set connections limit
	for i := 0; i < maxConns; i++ {
		h.Limit <- struct{}{}
	}

	return h
}

func ConnDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}
	query := `CREATE TABLE IF NOT EXISTS events (
		name varchar(255),
		time int(11) unsigned);`
	_, err = db.Exec(query)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func main() {
	db, err := ConnDB(dsn)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Connected to DB")

	//events to register
	events := map[string]bool{
		"a": true,
		"b": true,
		"c": true,
	}

	h := NewHandler(db, maxConns, events)

	r := mux.NewRouter()
	//register event
	r.HandleFunc("/events/{event}", h.RegisterLimit).Methods("POST")
	//get events
	r.HandleFunc("/events/{timefrom}/{timeto}", h.Get).Methods("GET")

	srv := &http.Server{
		Addr:         "0.0.0.0:8080",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	log.Println("Starting server at :8080")
	go func() {
		log.Println(srv.ListenAndServe())
	}()

	//Graceful Shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	srv.Shutdown(ctx)
	log.Println("Shutting down")
}
